//
//  RefreshContents.h
//  IMGR
//
//  Created by brst on 17/08/15.
//  Copyright (c) 2015 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RefreshContents : UIView
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *refreshIndicator;
@property (strong, nonatomic) IBOutlet UIImageView *pullDownImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;

@end
