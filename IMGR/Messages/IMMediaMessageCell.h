//
//  IMMediaMessageCell.h
//  IMGR
//
//  Created by Satendra Singh on 2/25/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMMessageArchiving_Message_CoreDataObject.h"

@interface IMMediaMessageCell : UITableViewCell

@property (nonatomic, strong) CMMessageArchiving_Message_CoreDataObject *message;
@property (weak, nonatomic) IBOutlet UIImageView *messageDeliveredImage;

@property (weak, nonatomic) IBOutlet UIImageView *mediaImageView;
@property (weak, nonatomic) IBOutlet UILabel *messageTimeLabel;
@property (strong, nonatomic) NSIndexPath* lastSelectedRowIndexPath;

+ (IMMediaMessageCell *)cellLoadedFromNibFile;

+ (NSString *) cellReusableIdentifier;
@property (weak, nonatomic) IBOutlet UIButton *resendButton;

- (IBAction)userDidTappedOnResend:(UIButton *)sender;
@end
