//
//  IMNoMessageView.m
//  IMGR
//
//  Created by Satendra Singh on 2/11/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMNoMessageView.h"

@implementation IMNoMessageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+ (IMNoMessageView *)viewLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            return (IMNoMessageView *)obj;
            break;
        }
    }
    return nil;
}

+ (NSUInteger) noMessageTag
{
    return 19865;
}

@end
