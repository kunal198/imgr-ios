//
//  IMPromoChoiceViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/13/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPromoChoiceViewController.h"
#import "IMSelectPersonalPromoController.h"
#import "IMSelectSponsoredAdsController.h"

@interface IMPromoChoiceViewController ()

@end

@implementation IMPromoChoiceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IMSelectSponsoredAdsController *selectPromoController = segue.destinationViewController;
    selectPromoController.selectionBlockObject = ^(IMPromos *selectedPromo){
        
        [self.navigationController popViewControllerAnimated:YES];
        self.selectionBlockObject(selectedPromo);
    };
}

@end
