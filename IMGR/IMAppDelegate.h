//
//  IMAppDelegate.h
//  IMGR
//
//  Created by Satendra Singh on 2/3/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TWTSideMenuViewController.h"
#import "IMContacts.h"
#import "IMPromos.h"

extern BOOL  messagesNotificationAlert;
@class CMNetManager;
@interface IMAppDelegate : UIResponder <UIApplicationDelegate, TWTSideMenuViewControllerDelegate>{
    NSString *notificationCheck;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController* navigationController;
@property (nonatomic, strong) NSDictionary *myVcard;
@property int unreadMessageCount;
@property (nonatomic)BOOL isRegistering;

- (UIStoryboard *)settingsStoryboard;
- (UIStoryboard *)contactsStoryboard;
- (UIStoryboard *)messagesStoryboard;
- (UIStoryboard *)registrationStoryboard;

- (void) login;
- (NSString*)getUserID;

+ (IMAppDelegate *)sharedDelegate;

+ (NSString *)jidForPhoneNumber:(NSString *)phoneNumber;

- (void) getPromos;
- (void) downloadPromoWithId:(NSUInteger) promId;
- (void) downloadPromoImageForPromo:(IMPromos *)promo;
- (void) refreshBlockContacts;
- (void) getSponsoredAds;
-(void)setSponsoredAds:(NSNumber *)promo_id is_Enable:(NSNumber *)is_Enable;
-(void)setPersonalAds:(NSNumber *)promo_id is_Enable:(NSNumber *)is_Enable;
- (void) getPersonalAds;
- (void) unBlockContact:(IMContacts *)contact;
- (void) blockContact:(IMContacts *)contact;
- (void) setUpUserProfile;
- (void) refreshThirdPartyPromos;
- (void) updateDeviceToken;

- (CMNetManager*)getIMGRImages: (id)target;
- (CMNetManager*)checkIMGRUsers: (NSMutableString*)formattedPhoneNumberList delegate: (id)delegate;

@end

extern IMAppDelegate* app;
extern NSString *notificationCount;