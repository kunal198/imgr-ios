//
//  IMUtils.h
//  IMGR
//
//  Created by akram on 04/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMUtils : NSObject


+ (float)getOSVersion;
+ (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage;
+ (UIImage*)circularScaleNCrop:(UIImage*)image rect: (CGRect) rect;
+ (UIImage*)resizeImage:(UIImage*)image toSize:(CGSize) sacleSize;

+ (NSString*)getImageFolderPath;
+ (void)displayActionSheet: (UIViewController*)controller;
+ (NSString *)phoneNumberForJid:(NSString *)jid;
+ (NSString *)phonenumberFromFormattedNumber:(NSString *) formatted;

//+ (BOOL)validateEmail:(NSString *)emailStr;
+(BOOL) validEmail:(NSString *)checkString;
+ (UIColor *) appDarkGreyColor;
+ (UIColor *) appBlueColor;
+ (UIColor *) appRedColor;

#pragma mark -
#pragma mark  App fonts methods
#pragma mark -

+ (UIFont *) appFontWithSize:(CGFloat) fontSize;
+ (UIFont *) appBoldFontWithSize:(CGFloat) fontSize;

+ (NSString *)myPhoneNumber;
+ (void)noInternetAvailable;
+ (CGSize) imageSizeAtFilePath:(NSString *) imagePath;
@end
