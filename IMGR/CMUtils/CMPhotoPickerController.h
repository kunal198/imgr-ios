//
//  CMPhotoPickerController.h
//  Radus
//
//  Created by Akram on 7/26/13.
//  Copyright (c) 2013 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMPhotoPickerController : UIImagePickerController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    
}
+ (CMPhotoPickerController *)sharedInstance;
- (void)displayImagePickerWithType:(NSInteger)pickerType target:(id)target;

@end
