//
//  CMNoInternetView.m
//  Radus
//
//  Created by saurabh on 7/30/13.
//  Copyright (c) 2013 test. All rights reserved.
//

#import "CMNoInternetView.h"

enum{
        E_NO_NET_VIEW_TAG = 9,
};
@implementation CMNoInternetView

static CMNoInternetView *sharedInstance;

+ (CMNoInternetView*)sharedInstance
{
    @synchronized(self)
    {
        if(!sharedInstance)
        {
            [[CMNoInternetView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
        }
    }
    return sharedInstance;
}

+(id)alloc
{
    @synchronized(self)
    {
        NSAssert(sharedInstance == nil, @"Attempted to allocate a second instance of a singleton FAWDataController.");
        sharedInstance = [super alloc];
    }
    
    return sharedInstance;
}
-(void)dealloc{
    
    _RELEASE(sharedInstance)
    [super dealloc];
}
-(void)initializeView{

    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor clearColor];
    
    UIImage* noInternetBar = [CSUtils imageWithName:@"NoNetBar"];
    UIImageView* noInternetBarView = [CSUtils imageViewWithUIImage:noInternetBar imageID:E_NO_NET_VIEW_TAG andFrame:CGRectMake(0, 0, SCREEN_WIDTH, 43)];
    noInternetBarView.alpha = 0.8;
    [self addSubview:noInternetBarView];
    
    UIImage* noNetIconImage = [CSUtils imageWithName:@"NoNetIcon"];
    UIImageView* noNetIconView = nil;

    noNetIconView = [CSUtils imageViewWithUIImage:noNetIconImage imageID:0 andFrame:CGRectMake(50, (43 - noNetIconImage.size.height)/2, noNetIconImage.size.width, noNetIconImage.size.height)];
       
    [self addSubview:noNetIconView];
    
    UILabel* titleLabel = [CSUtils labelWithTitle:NO_INTERNET_CONNECTION labelId:100 labelFrame:CGRectMake(noNetIconView.frame.origin.x + noNetIconView.frame.size.width + 5, 0, 300, 43) textColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:ROCKWELL_FONT size: IS_IPHONE?14.0: 17.0]];
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:titleLabel];
    
}
-(void)showOnView:(UIView*)view{
    
    if (![self viewWithTag:E_NO_NET_VIEW_TAG]) {
        [self initializeView];
    }
    if (!self.superview || self.superview != view) {
        [self removeFromSuperview];
        //[self initializeView];
        [view addSubview:self];
        [view bringSubviewToFront:self];
        
        [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            self.frame = CGRectMake(0, 0, SCREEN_WIDTH, 43);
            
        }completion:^(BOOL finished){
            
            
        }];
    }
    
}
-(void)showOnView:(UIView*)view withYPos:(CGFloat)yPos{
    
    if (![self viewWithTag:E_NO_NET_VIEW_TAG]) {
        
        [self initializeView];
    }
    //if (!self.superview || self.superview != view) {
        self.frame = CGRectMake(0, yPos, SCREEN_WIDTH, 0);
        [self removeFromSuperview];
       // [self initializeView];
        [view addSubview:self];
        [view bringSubviewToFront:self];
        
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            self.frame = CGRectMake(0, yPos, SCREEN_WIDTH, 43);
            
        }completion:^(BOOL finished){
            
            
        }];
   // }
    
}
-(void)hideOnView:(UIView*)view{
    
    if (self.superview == view) {
        
        [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            self.frame = CGRectMake(0, self.frame.origin.y, SCREEN_WIDTH, 0);
            
        }completion:^(BOOL finished){
            [self removeFromSuperview];
            
        }];
    }
    
}
-(void)hide{
    
    [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.frame = CGRectMake(0, self.frame.origin.y, SCREEN_WIDTH, 0);
        
    }completion:^(BOOL finished){
        [self removeFromSuperview];
        
    }];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self hide];
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    
    [super touchesEnded:touches withEvent:event];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
