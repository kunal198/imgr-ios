//
//  IMPromoHeaderView.m
//  IMGR
//
//  Created by Satendra Singh on 2/11/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPromoHeaderView.h"

@implementation IMPromoHeaderView

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:[IMPromoHeaderView cellReusableIdentifier]];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (IMPromoHeaderView *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            return (IMPromoHeaderView *)obj;
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMPromoHeaderView";
}

@end
