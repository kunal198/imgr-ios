//
//  IMAddPromoTableCell.h
//  IMGR
//
//  Created by Satendra Singh on 2/5/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMTextFieldTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *contentTextField;

+ (IMTextFieldTableCell *)cellLoadedFromNibFile;

+ (NSString *) cellReusableIdentifier;

@end
