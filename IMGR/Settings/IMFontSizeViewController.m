//
//  IMFontSizeViewController.m
//  IMGR
//
//  Created by Satendra Singh on 2/6/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMFontSizeViewController.h"
#import "IMCheckMarkTableCell.h"
#import "IMUtils.h"


@interface IMFontSizeViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *tableBGImageView;

@end

@implementation IMFontSizeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundView = self.tableBGImageView;
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    NSUInteger selected = 0;
//    if (k_LARGE_FONT_SIZE == [[NSUserDefaults standardUserDefaults] integerForKey:k_DEFAULTS_KEY_FONT_SIZE]) {
//        selected = 1;
//    }
//    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selected inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUInteger selected = 0;
    if (k_LARGE_FONT_SIZE == [[NSUserDefaults standardUserDefaults] integerForKey:k_DEFAULTS_KEY_FONT_SIZE]) {
        selected = 1;
    }
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selected inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section) {
        return 0;
    }
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    return 22.00;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMCheckMarkTableCell *cell = [tableView dequeueReusableCellWithIdentifier:[IMCheckMarkTableCell cellReusableIdentifier]];
    if (nil == cell) {
        cell = [IMCheckMarkTableCell cellLoadedFromNibFile];
    }
    // Configure the cell...
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Normal";
            cell.textLabel.font = [IMUtils appFontWithSize:k_NORMAL_FONT_SIZE];
            break;
            
        case 1:
            cell.textLabel.text = @"Large";
            cell.textLabel.font = [IMUtils appFontWithSize:k_LARGE_FONT_SIZE];

            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row) {
        [[NSUserDefaults standardUserDefaults] setInteger:k_LARGE_FONT_SIZE forKey:k_DEFAULTS_KEY_FONT_SIZE];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setInteger:k_NORMAL_FONT_SIZE forKey:k_DEFAULTS_KEY_FONT_SIZE];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    }
//    else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }
//}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */
@end
