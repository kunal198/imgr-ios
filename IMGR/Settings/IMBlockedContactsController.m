//
//  IMBlockedContactsController.m
//  IMGR
//
//  Created by Satendra Singh on 2/5/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMBlockedContactsController.h"
#import "IMContactsViewController.h"
#import "IMContacts.h"
#import "CMCoreDataHandler.h"
#import "IMSelectUserViewController.h"
#import "IMDataHandler.h"

@interface IMBlockedContactsController ()<NSFetchedResultsControllerDelegate>
{
    NSFetchedResultsController *fetchedResultsController;
}
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *tableCustomFooter;
@property (weak, nonatomic) IBOutlet UIView *noBlockContactView;

- (IBAction)userDidEdit:(id)sender;
- (IBAction)userDidTappedAddNew:(id)sender;

@end

@implementation IMBlockedContactsController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSFetchedResultsController*)fetchedResultsController
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    if(fetchedResultsController == nil)
    {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMContacts"
                                                  inManagedObjectContext:moc];
        
//        NSSortDescriptor *sd1 = [[NSSortDescriptor alloc] initWithKey:@"index_character" ascending:YES];
        NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"first_name" ascending:YES];
        
        NSArray *sortDescriptors = [NSArray arrayWithObjects: sd2, nil];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        [fetchRequest setSortDescriptors:sortDescriptors];

            NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_blocked == %d", YES];
            [fetchRequest setPredicate: predicate];

        fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                       managedObjectContext:moc
                                                                         sectionNameKeyPath:nil
                                                                                  cacheName:nil];
        [fetchedResultsController setDelegate:self];
        
        NSError *error = nil;
        if (![fetchedResultsController performFetch:&error])
        {
            DDLogError(@"Error performing fetch: %@", error);
        }
    }
    return fetchedResultsController;
}

#pragma mark - core data delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundView = _backgroundImageView;
    self.tableView.tableFooterView = self.tableCustomFooter;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated
{
 }
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setTitle:@"Add New +" forState:UIControlStateNormal];
////    self.tableView.tableFooterView = button;
//    return button;
//}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSUInteger objects = [[[self fetchedResultsController] fetchedObjects] count];
    if (objects) {
        if ([self.noBlockContactView superview]) {
            [self.noBlockContactView removeFromSuperview];
        }
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    else
    {
        [self.view addSubview:self.noBlockContactView];
        CGRect noViewFrame = self.noBlockContactView.frame;
        noViewFrame.origin.y = self.view.frame.size.height/2 - noViewFrame.size.height/2 - 64 + 45.00 ;
        self.noBlockContactView.frame = noViewFrame;
        self.navigationItem.rightBarButtonItem.enabled = NO;

    }

    return objects;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    IMContacts *contact = [[self fetchedResultsController] objectAtIndexPath:indexPath];

    cell.textLabel.text = [contact first_name];
    // Configure the cell...
    
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 20.0;
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
//    
//}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Unblock";
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        IMContacts *contact = [[self fetchedResultsController] objectAtIndexPath:indexPath];
//        contact.is_blocked = [NSNumber numberWithBool:NO];
//        contact.is_imgr_user = [NSNumber numberWithBool:YES];
//        [[IMDataHandler sharedInstance] saveDatabase];
//        [[CMChatManager sharedInstance] addBuddy:contact.user_id inHost:HOST_OPENFIRE withNickName:contact.first_name];
//        [[IMAppDelegate sharedDelegate] refreshBlockContacts];
        [[IMAppDelegate sharedDelegate] unBlockContact:contact];

    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

- (IBAction)userDidEdit:(UIBarButtonItem *)sender {
    
    [self.tableView setEditing:!self.tableView.editing animated:YES];
    
    if (self.tableView.editing) {
        [sender setTitle:@"Done"];
    }
    else{
        [sender setTitle:@"Edit"];
    }

}

- (IBAction)userDidTappedAddNew:(id)sender {

    [self performSegueWithIdentifier:@"IMSelectUserViewController" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"IMSelectUserViewController"]) {
        __block IMSelectUserViewController *contacts = [segue destinationViewController];
        contacts.selectionBlockObject = ^(NSString *jid){
            IMContacts *contact = [[IMDataHandler sharedInstance] fetchContactWithUserJid:jid];
            [[IMAppDelegate sharedDelegate] blockContact:contact];
//            [[CMChatManager sharedInstance] deleteAllMessagesWithHost:HOST_OPENFIRE forJID:contact.user_id];
//            contact.is_blocked = [NSNumber numberWithBool:YES];
//            contact.is_imgr_user = [NSNumber numberWithBool:NO];
//            [[IMDataHandler sharedInstance] saveDatabase];
//            [[CMChatManager sharedInstance] removeBuddyInHost:HOST_OPENFIRE withJid:contact.user_id];
//            [[IMAppDelegate sharedDelegate] refreshBlockContacts];

        };
    }
}

@end
