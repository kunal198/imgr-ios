//
//  IMSelectUserViewController.h
//  IMGR
//
//  Created by Satendra Singh on 2/12/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//
#import "IMContactsViewController.h"

#import <UIKit/UIKit.h>

typedef void (^SelectionBlock)(NSString *selectedJID);

@interface IMSelectUserViewController : IMContactsViewController

@property (copy)  SelectionBlock selectionBlockObject;

@end
