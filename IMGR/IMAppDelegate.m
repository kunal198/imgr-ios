 //
//  IMAppDelegate.m
//  IMGR
//
//  Created by Satendra Singh on 2/3/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMAppDelegate.h"
#import "IMSettingsViewController.h"
#import "IMMenuViewController.h"
#import "TWTSideMenuViewController.h"
#import "IMDataHandler.h"
#import "IMChatViewController.h"

#import "CMCoreDataHandler.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "CMMessageArchivingCoreDataStorage.h"
#import "XMPPCoreDataStorageProtected.h"
#import "KeychainItemWrapper.h"
#import "CMChatManager.h"
#import "CMStream.h"
#import "CMMessageArchiving_Message_CoreDataObject.h"
#import "NSString+Emoticon.h"
#import "XMPPStream.h"
#import "XMPP.h"
IMAppDelegate *app = nil;
NSString *notificationCount =nil;

BOOL  messagesNotificationAlert;

@interface IMAppDelegate () <CMNetManagerDelegate,CMChatManagerDelegate,NSFetchedResultsControllerDelegate,IMDownloadPictureOperationDelegate, CMChatManagerMessagesDelegate,XMPPStreamDelegate,CMMessageArchivingCoreDataStorageDelegate>
{
    
    
    UIStoryboard *settingsStoryboard;
    UIStoryboard *contactsStoryboard;
    UIStoryboard *messagesStoryboard;
    UIStoryboard *registrationStoryboard;

    IMOperationQueue* queueOperation;
    
    MBProgressHUD*      progressView;
    
    int                 badge_value;
    
    BOOL                isFromBackground;
    BOOL                isAppActive;
    BOOL                isPushUnderProcess;
    
    NSDictionary*       lastNotification;
    NSMutableString*    phoneNumberList;
    __block NSMutableSet *blockedJids;
    
    
    
}

@property (nonatomic, strong) TWTSideMenuViewController *sideMenuViewController;
@property (nonatomic, strong) IMMenuViewController *menuViewController;
@property (nonatomic, weak)id delegate;

@end

@implementation IMAppDelegate
@synthesize isRegistering;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DebugLog(@"didFinishLaunchingWithOptions %@", launchOptions);
    
    
//    
//    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound);
//    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
//                                                                             categories:nil];
//    [application registerUserNotificationSettings:settings];
//    [application registerForRemoteNotifications];
//    
    
    
    [[CMCoreDataHandler sharedInstance] managedObjectContext];//Initialize the manage object context for future use

    NSString *IMTextAttributeTextColor = nil;
    NSString *IMTextAttributeFont = nil;
    if([IMUtils getOSVersion] >= 7.0)
    {
        IMTextAttributeTextColor    = NSForegroundColorAttributeName;
        IMTextAttributeFont         = NSFontAttributeName;
    }
    else{
        IMTextAttributeTextColor    = UITextAttributeTextColor;
        IMTextAttributeFont         = UITextAttributeFont;
    }
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            IMTextAttributeTextColor: [IMUtils appDarkGreyColor],
                                                            IMTextAttributeFont: [IMUtils appFontWithSize:19.0],
                                                            }];
    [[UINavigationBar appearance] setTintColor:[IMUtils appBlueColor]];
    if ([IMUtils getOSVersion] <7.0) {
        [[UINavigationBar appearance] setTitleVerticalPositionAdjustment:+3.0 forBarMetrics:UIBarMetricsDefault];
    }
    
    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    
    queueOperation = [[IMOperationQueue alloc] init];

    isFromBackground = NO;
    isAppActive = YES;
    app = self;
    
    [[CMChatManager sharedInstance] setDelegate:self];
    [[CMChatManager sharedInstance] setMessagesDelegate: self];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerationFinished:) name: @"REGISTRATION_COMPLETED" object: nil];
    
    //if(![[NSUserDefaults standardUserDefaults] objectForKey: K_DEVICE_TOKEN])
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (nil == [defaults objectForKey:k_DEFAULTS_KEY_USER_ID]) {//First Time operation
        isRegistering = YES;
        NSLog(@"if (nil == [defaults objectForKey:k_DEFAULTS_KEY_PROMOS])");
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_PROMOS];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_NOTIFICATIONS];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_ROTATE_PERSONAL];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_ROTATE_ENABLED];
        [defaults setBool:YES forKey:k_DEFAULTS_KEY_ROTATE_SPONSORED];
        [defaults setBool:NO forKey:k_DEFAULTS_KEY_OVERRIDE_FRIEND_BUBBLE_COLOR];
        
        KeychainItemWrapper * keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"com.imagemessenger.imgr" accessGroup:nil];
         NSString* udid = [keychain objectForKey:(__bridge id)(kSecAttrAccount)];
        if(udid.length) {
            
            [defaults setObject:udid forKey:K_DEFAULTS_DEVICE_UDID];
            
        }
        else {
            NSUUID* NSUUID = [[UIDevice currentDevice] identifierForVendor];
            NSString* udid = [[NSUUID UUIDString] lowercaseString];
            if(udid != nil)
                [defaults setObject:udid forKey:K_DEFAULTS_DEVICE_UDID];
            
            [keychain setObject:udid forKey:(__bridge id)(kSecAttrAccount)];
            
        }
        [defaults synchronize];

        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
    else
    {
        DebugLog(@"before login %@", launchOptions);

        [self login];
        //[self getPromos];
        DebugLog(@"after login %@", launchOptions);

        
        UINavigationController* navigationController = [[self messagesStoryboard] instantiateInitialViewController];
        [self registerationFinished: [NSNotification notificationWithName: @"REGISTRATION_COMPLETED" object: navigationController]];
        
        
    }
    
    progressView = nil;//SS we do not need it
    progressView = [[MBProgressHUD alloc] init];
    progressView.labelText = @"Please wait...";
    
    [self.window makeKeyAndVisible];
    // Override point for customization after application launch.
    NSDictionary* userInfo = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    DebugLog(@"userInfo remote notification = %@", userInfo);
    
    if([launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey])
    {
        DebugLog(@"called from UIApplicationLaunchOptionsRemoteNotificationKey");

        //isFromBackground = NO;

        [progressView setFrame: app.window.frame];
        [app.window addSubview: progressView];
        [progressView show: YES];
        [self performSelector:@selector(lauchChatScreen:) withObject:userInfo afterDelay:2];
    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    DebugLog(@"applicationWillResignActive");

    //[[CMChatManager sharedInstance] goOfflineForHost:HOST_OPENFIRE];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
   
    DebugLog(@"applicationWillTerminate");

    [[CMChatManager sharedInstance] goOfflineForHost:HOST_OPENFIRE];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotification:[NSNotification notificationWithName:@"appWillTerminate" object:nil]];
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *_deviceToken = [[[[deviceToken description]
                                stringByReplacingOccurrencesOfString:@"<"withString:@""]
                               stringByReplacingOccurrencesOfString:@">" withString:@""]
                              stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"Device Token = %@",_deviceToken);
    
    if (_deviceToken) {
        
        [[NSUserDefaults standardUserDefaults] setObject:_deviceToken forKey:K_DEVICE_TOKEN];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self updateDeviceToken];
    }
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
{
    
    
//    
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//    NSLog(@"hhhhhhhhhhhh%@",userInfo);
//    
//    
//    
//    
//    if (application.applicationState == UIApplicationStateActive)
//    {
//        
//        NSLog(@"Push Received during online now ");
//        
//    }
//    else
//    {
//        
////        CMStream  *stream = [[CMStream alloc]init];
////        
////        
////        [stream addDelegate:self delegateQueue:dispatch_get_main_queue()];
////        stream.autoStartTLS = YES;
////        
////        CMReconnect *xmppReconnect;
////        xmppReconnect = [[CMReconnect alloc]init];
////        [xmppReconnect activate:stream];
////        
////        
////            
////        
////        
////        CMMessageArchivingCoreDataStorage    *xmppMessageArchivingCoreDataStorage = [CMMessageArchivingCoreDataStorage sharedInstance];
////        CMMessageArchiving    *xmppMessageArchivingModule = [[CMMessageArchiving alloc]initWithMessageArchivingStorage:xmppMessageArchivingCoreDataStorage];
////        [xmppMessageArchivingModule setClientSideMessageArchivingOnly:YES];
////        [xmppMessageArchivingModule activate:stream];    //By this line all your messages are stored in CoreData
////        [xmppMessageArchivingModule addDelegate:self delegateQueue:dispatch_get_main_queue()];
//        
//                    
//
//        if(![[app.navigationController topViewController] isKindOfClass: [IMChatViewController class]])
//        {
//            [progressView setFrame: app.window.frame];
//            [app.window addSubview: progressView];
//            [progressView show: YES];
//        }
//        if(!isPushUnderProcess)
//        {
//            isPushUnderProcess = YES;
//            
//            //[self performSelector:@selector(lauchChatScreen:) withObject:userInfo afterDelay:0.5];
//           // [self performSelectorOnMainThread:@selector(lauchChatScreen:) withObject:userInfo waitUntilDone:NO];
//            [self lauchChatScreen:userInfo];
//        }
//    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
//    [[NSUserDefaults standardUserDefaults] setObject:@"936eed4a-eee8-4525-9bdd-0af94bb7e99" forKey:K_DEVICE_TOKEN];
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@",str);
}

//- (void) clearNotifications {
//    int badgeNum = [[UIApplication sharedApplication] applicationIconBadgeNumber];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNum];
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [[UIApplication sharedApplication] cancelLocalNotification: notification];
    if (application.applicationState == UIApplicationStateActive) {
        
        NSLog(@"didReceiveLocalNotification during online");
        return;
    }
    
    if(![[app.navigationController topViewController] isKindOfClass: [IMChatViewController class]])
    {
        [progressView setFrame: app.window.frame];
        [app.window addSubview: progressView];
        [progressView show: YES];
    }
    if(!isPushUnderProcess) {
        isPushUnderProcess = YES;
        badge_value = notification.applicationIconBadgeNumber;
        //[self performSelector:@selector(lauchChatScreen:) withObject:notification.userInfo afterDelay:0.5];
        
    }
}

- (void)lauchChatScreen: (NSDictionary*)userInfo
{
    NSLog(@"User Info = %@", userInfo);
    for (id key in userInfo)
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    
    //NSString* senderJid = [[[userInfo objectForKey: @"aps"] objectForKey: @"pushData"] objectForKey: @"sender_id"];
    NSString* senderJid = nil;
    if([userInfo objectForKey: @"sender_id"]) {
        senderJid = [userInfo objectForKey: @"sender_id"];
    
    }
    else
    
    {
        senderJid = [[[userInfo objectForKey: @"aps"] objectForKey: @"pushData"] objectForKey: @"sender_id"];
        badge_value = [[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue];
    }
    
    NSLog(@"senderJid=%@ badge_value=%d",senderJid,badge_value);
    IMChatViewController* chatVC = nil;
    NSLog(@"Top View Controller = %@", [app.navigationController topViewController]);
    if([[app.navigationController topViewController] isKindOfClass: [IMChatViewController class]])
    {
        NSLog(@"[[app.navigationController topViewController] isKindOfClass: [IMChatViewController class]]");
        chatVC = (IMChatViewController*)[app.navigationController topViewController];
        //if(![chatVC.userJid isEqualToString: senderJid]) {
            chatVC.userJid = senderJid;
        
        NSLog(@"dffddfdsfdsfd%@",chatVC.userJid);
        
            [chatVC refreshViewForSwitchChat];
        
            [progressView hide: YES];
        //}
        isPushUnderProcess = NO;
        return;
    }
    isPushUnderProcess = NO;
    
    [progressView hide: YES];
    
    
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    isAppActive = YES;
    DebugLog(@"applicationDidBecomeActive");
    
   
    
    
    
    
    
    UINavigationController* navC = [[self messagesStoryboard] instantiateInitialViewController];
    
    [self registerationFinished: [NSNotification notificationWithName: @"REGISTRATION_COMPLETED" object: navC]];
    
    chatVC = [[self messagesStoryboard] instantiateViewControllerWithIdentifier: @"IMChatViewController"];
    
    app.isRegistering = NO;
    chatVC.userJid = [senderJid stringByReplacingOccurrencesOfString:HOST_OPENFIRE withString: HOST_OPENFIRE];
    
    [navC pushViewController: chatVC animated: NO];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr%@",userInfo);
    
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if (application.applicationState == UIApplicationStateActive)
    {
        

    }
    else
        
    {
        
        if(![[app.navigationController topViewController] isKindOfClass: [IMChatViewController class]])
        {
            [progressView setFrame: app.window.frame];
            [app.window addSubview: progressView];
            [progressView show: YES];
            

           
        }
        if(!isPushUnderProcess)
        {
           
            
            isPushUnderProcess = YES;
            //[progressView show: YES];
            [self performSelector:@selector(lauchChatScreen:) withObject:userInfo afterDelay:1];
        }
        
        
    }
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
   
    
    
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    
    [center postNotification:[NSNotification notificationWithName:@"appDidEnterForeground" object:nil]];
    [[CMChatManager sharedInstance] goOfflineForHost:HOST_OPENFIRE];

//    NSLog(@"%@",notificationCount);
//    
//    if ([notificationCount isEqualToString:@"Message"]) {
//        
//    }else{
//    
//    IMChatViewController *chatVC = [[IMChatViewController alloc]init];
//    [chatVC notificationCount];
//    }
//    isAppActive = NO;
//    //[[CMChatManager sharedInstance] goOfflineForHost:HOST_OPENFIRE];//by akram tag missing message
//    [[CMChatManager sharedInstance] setMessagesDelegate: self];
//    
//    NSLog(@"applicationDidEnterBackground %@: %@", THIS_FILE, THIS_METHOD);
//    
//#if TARGET_IPHONE_SIMULATOR
//    NSLog(@"The iPhone simulator does not process background network traffic. "
//               @"Inbound traffic is queued until the keepAliveTimeout:handler: fires.");
//#endif
//    
//    //__block CMStream *stream = nil;
//    
//    UIBackgroundTaskIdentifier task = 0;
//    UIApplication*    app = [UIApplication sharedApplication];
//    task = [app beginBackgroundTaskWithExpirationHandler:^{
//        
//        
//        [app endBackgroundTask:task];
//        
//    }];
//    // Start the long-running task and return immediately.
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        // Do the work associated with the task.
//        NSLog(@"Started background task timeremaining = %f", [app backgroundTimeRemaining]);
//        // do work son...
//        
//        [[CMChatManager sharedInstance] goOfflineForHost:HOST_OPENFIRE];
//        [app endBackgroundTask:task];
//    });
//    
//    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
//    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    DebugLog(@"applicationWillEnterForeground");
    
    CMStream *stream = [[CMChatManager sharedInstance] getStreamForHost:HOST_OPENFIRE];
    NSLog(@"stream in enter forground = %@", stream);
    if(![stream isConnected])
    {
        [stream isConnected];
        //[self login];
//        if ( ![stream isConnecting])
//        {
//            [self login];
//        }
        [[CMChatManager sharedInstance] goOnlineForHost:HOST_OPENFIRE];
    }
    else
    {
        [[CMChatManager sharedInstance] goOnlineForHost:HOST_OPENFIRE];
    }
//    else if(![stream isConnecting])
//    {
//        [[CMChatManager sharedInstance] goOnlineForHost:HOST_OPENFIRE];
//    }
    if(stream == nil)
    {
        [self login];
    }
    
    isAppActive = YES;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
   

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    
    
    
    CMStream *stream = [[CMChatManager sharedInstance] getStreamForHost:HOST_OPENFIRE];
    NSLog(@"stream in didbecomeactive method = %@", stream);
    if ([stream isDisconnected])
    {
        [stream  disconnect];
    }
    if(stream == nil) {
        [self login];
    }
    [[CMChatManager sharedInstance] goOnlineForHost:HOST_OPENFIRE];
    if(isFromBackground && lastNotification)
    {
        isFromBackground = NO;
        [self lauchChatScreen: lastNotification];
        lastNotification = nil;
    }
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

}


- (void)registerationFinished: (NSNotification*)notif
{
    self.menuViewController = [[IMMenuViewController alloc] initWithNibName:nil bundle:nil];
    //self.mainViewController = [[TWTMainViewController alloc] initWithNibName:nil bundle:nil];
    
    UINavigationController* navC = (UINavigationController*)[notif object];
    
    app.navigationController = navC;
    DebugLog(@"Controller = %@", navC.viewControllers);
    self.sideMenuViewController = [[TWTSideMenuViewController alloc] initWithMenuViewController:self.menuViewController mainViewController:navC];
    
    if([IMUtils getOSVersion] >= 7.0) {}
    else {
        [navC.navigationBar setBackgroundImage: [UIImage imageNamed: @"navbar_small.png"] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor darkGrayColor],UITextAttributeTextColor,[UIFont fontWithName:@"Arial" size:17.0], UITextAttributeFont, nil]];
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        //[[[UIApplication sharedApplication] keyWindow] setBackgroundColor: [UIColor whiteColor]];
        
        [[UINavigationBar appearance] setTintColor:[IMUtils appBlueColor]];
        //[[UIApplication sharedApplication] setStatusBarStyle:<#(UIStatusBarStyle)#>]
        
        
       // navC.navigationItem.leftBarButtonItems
    }
    self.sideMenuViewController.shadowColor = [UIColor blackColor];
    self.sideMenuViewController.edgeOffset = (UIOffset) { .horizontal = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 18.0f : 0.0f };
    self.sideMenuViewController.zoomScale = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 0.5634f : 0.85f;
    self.sideMenuViewController.delegate = self;
    app.window.rootViewController = self.sideMenuViewController;
}

- (void)getSponsoredAds
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *jidString = [app getUserID];//[[NSUserDefaults standardUserDefaults] valueForKey:k_DEFAULTS_KEY_USER_ID];
    
    //NSString* jidString = [IMAppDelegate jidForPhoneNumber:userId];
    DebugLog(@"jidString = %@", jidString);
    
    [parameters setObject:jidString forKey: @"username"];
    [parameters setObject:@"0" forKey: @"isPersonal"];
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kGetPromoList] parameters:parameters queueIdentifier:"promoQueue" queueType:SERIAL httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_GET_PROMO_LIST_SERVICE;
    [netManager startRequest];
}

- (void)getPersonalAds
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *jidString = [app getUserID];//[[NSUserDefaults standardUserDefaults] valueForKey:k_DEFAULTS_KEY_USER_ID];

    //NSString* jidString = [IMAppDelegate jidForPhoneNumber:userId];
    DebugLog(@"jidString = %@", jidString);
    
    [parameters setObject:jidString forKey: @"username"];
    [parameters setObject:@"1" forKey: @"isPersonal"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                   ^{
                      // [self performSelector:@selector(methodThatloadYourData)];
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          //[self.tableView reloadData];
                                      });
                   });
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kGetPromoList] parameters:parameters queueIdentifier:"personalPromoQueue" queueType:SERIAL httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_GET_PROMO_LIST_SERVICE;
    [netManager startRequest];
}

-(void)setSponsoredAds:(NSNumber *)promo_id is_Enable:(NSNumber *)is_Enable
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *jidString = [app getUserID];
    DebugLog(@"jidString = %@", jidString);
    
    [parameters setObject:jidString forKey: @"username"];
    [parameters setObject:[NSString stringWithFormat:@"%@",is_Enable] forKey: @"is_enable"];
    [parameters setObject:[NSString stringWithFormat:@"%@",promo_id] forKey: @"promo_id"];

    
    NSLog(@"parameter=%@",parameters);
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kSetPromoStatus] parameters:parameters queueIdentifier:"setSponsorePromoQueue" queueType:SERIAL httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_SET_SPONSORE_PROMO_STATUS;
    [netManager startRequest];

}

-(void)setPersonalAds:(NSNumber *)promo_id is_Enable:(NSNumber *)is_Enable
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *jidString = [app getUserID];
    DebugLog(@"jidString = %@", jidString);
    
    [parameters setObject:jidString forKey: @"username"];
    [parameters setObject:[NSString stringWithFormat:@"%@",is_Enable] forKey: @"is_enable"];
    [parameters setObject:[NSString stringWithFormat:@"%@",promo_id] forKey: @"promo_id"];
    
    
    NSLog(@"parameter=%@",parameters);
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kSetPromoStatus] parameters:parameters queueIdentifier:"setPersonalPromoQueue" queueType:SERIAL httpMethod: GET];
    netManager.delegate = self;
    netManager.tag = E_SET_SPONSORE_PROMO_STATUS;
    [netManager startRequest];
    
}

- (void)updateDeviceToken
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *username = [[NSUserDefaults standardUserDefaults] valueForKey:k_DEFAULTS_KEY_USER_ID];
    NSString* deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEVICE_TOKEN];

    if(username.length && deviceToken.length)
    {
        [parameters setObject:username forKey: @"username"];
        [parameters setObject: deviceToken forKey: @"deviceToken"];
    
        NSString* udid = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEFAULTS_DEVICE_UDID];
        
        [parameters setObject:udid forKey: @"deviceId"];
        
         NSLog(@"updateDeviceToken Parameters = %@ and URL = %@", parameters, [NSString stringWithFormat: @"%@%@", KBASEURL, kUpdateDeviceToken]);
        
        CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kUpdateDeviceToken] parameters:parameters queueIdentifier:"personalPromoQueue" queueType:CONCURRENT httpMethod: GET];
        netManager.delegate = self;
        netManager.tag = E_UPDATE_DEVICE_TOKEN_SERVICE;
        [netManager startRequest];
    }
}

- (void) downloadPromoWithId:(NSUInteger) promId
{
        NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
        [parameters setObject:KAPIKEY forKey:@"apiKey"];
        
        NSString *userId = [app getUserID];//[[NSUserDefaults standardUserDefaults] valueForKey:k_DEFAULTS_KEY_USER_ID];
        
        NSString* jidString = [IMAppDelegate jidForPhoneNumber:userId];
        DebugLog(@"jidString = %@", jidString);
        
        //    [parameters setObject:jidString forKey: @"username"];
        //    [parameters setObject:@"0" forKey: @"isPersonal"];
        
        CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@%lu", KBASEURL, kGetPromWithID,(unsigned long)promId] parameters:parameters queueIdentifier:"promoQueue" queueType:SERIAL httpMethod: GET];
        netManager.delegate = self;
        netManager.tag = E_GET_PERSONAL_PROM_OTHER_USER;
        [netManager startRequest];
   
}
#pragma mark - CMNetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    NSLog(@"Response = %@", dictionary);
    switch (netManager.tag)
    {
           
        case E_SET_SPONSORE_PROMO_STATUS:
        {
            [[CMCoreDataHandler sharedInstance] performAndSaveDataBaseTaskInBackground:^{
               // NSArray* array = [dictionary objectForKey: @"data"];
                NSLog(@"prom data=%@",dictionary);
                
            } completionHandler:^{
                //NSArray* array = [dictionary objectForKey: @"data"];
                NSLog(@"prom data=%@",dictionary);
                
            }];
            
        }
            
            
            break;

        
        case E_GET_PROMO_LIST_SERVICE:
        {
            [[CMCoreDataHandler sharedInstance] performAndSaveDataBaseTaskInBackground:^{
                NSArray* array = [dictionary objectForKey: @"data"];
                // NSLog(@"prom data=%@",array);
                for (int i = 0; i < [array count]; ++i)
                {
                    NSDictionary* dict = [array objectAtIndex: i];
                    [[IMDataHandler sharedInstance] parsePromos: dict promoType: 1];
                }
            } completionHandler:^{
                NSArray* array = [dictionary objectForKey: @"data"];
                //NSLog(@"prom data=%@",array);
                for (int i = 0; i < [array count]; ++i)
                {
                    NSDictionary* dict = [array objectAtIndex: i];
                    
                    __block IMPromos* promo = [[IMDataHandler sharedInstance] getPromoInfoStoredInBackground: [dict objectForKey: @"promo_id"]];
                    
                    if(promo.promo_id ==[NSNumber numberWithInt:101])
                    {
                        NSLog(@"promo.is_enabled=%d",promo.is_enabled.boolValue);
                    }
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        if (promo) {
//                           // [self downloadPromoImageForPromo:promo];
//                        }
//                    });
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                                   ^{
                                        [self downloadPromoImageForPromo:promo];
                                       //[self performSelector:@selector(methodThatloadYourData)];
                                       dispatch_async(dispatch_get_main_queue(),
                                                      ^{
                                                         
                                                      });
                                   });
                }

            }];
            
        }
            
            
            break;
            
        case E_GET_PERSONAL_PROM_OTHER_USER:
        {
                [[CMCoreDataHandler sharedInstance] performAndSaveDataBaseTaskInBackground:^{
                    NSArray* array = [dictionary objectForKey: @"data"];
                    for (int i = 0; i < [array count]; ++i)
                    {
                        NSDictionary* dict = [array objectAtIndex: i];
//                        IMPromos* promo = [[IMDataHandler sharedInstance] fetchPromoWithId: [[dict objectForKey: @"promo_id"] intValue]];
                        //SS parse it always for newly changes applied for promo
                        //                    if (nil == promo)
                        {
                            if ([[dict objectForKey:@"promo_type"] integerValue] == 1)// For personal promo
                            {
                                NSMutableDictionary *dictMutable = [dict mutableCopy];
                                [dictMutable setValue:[NSNumber numberWithInt:99] forKey:@"promo_type"];
                                [[IMDataHandler sharedInstance] parsePromos: dictMutable promoType: 1];
                            }
                            else
                            {
                                [[IMDataHandler sharedInstance] parsePromos: dict promoType: 1];
                            }
                        }
                    }
                } completionHandler:^{
                    NSArray* array = [dictionary objectForKey: @"data"];

                    for (int i = 0; i < [array count]; ++i)
                    {
                        NSDictionary* dict = [array objectAtIndex: i];
                             IMPromos* promo = [[IMDataHandler sharedInstance] getPromoInfoStoredInBackground: [dict objectForKey: @"promo_id"] ];
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                                       ^{
                                           [self downloadPromoImageForPromo:promo];
                                           //[self performSelector:@selector(methodThatloadYourData)];
                                           dispatch_async(dispatch_get_main_queue(),
                                                          ^{
                                                              
                                                          });
                                      });
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [self downloadPromoImageForPromo:promo];
//                            });
                    }
   
                }];
        }break;
            
        case E_CHECK_IMGR_SERVICE:
        {
          __block NSString * oldJID = nil;
            [[CMCoreDataHandler sharedInstance] performAndSaveDataBaseTaskInBackground:^{
                NSArray* array = [dictionary objectForKey: @"data"];
                for (int i = 0; i < [array count]; ++i)
                {
                        NSDictionary* dict = [array objectAtIndex: i];

                        IMContacts* contact = [[IMDataHandler sharedInstance] fetchContactInBackgroundWithPhoneNumber: [dict objectForKey: @"phonenumber"]];
                        if ([[dict objectForKey: @"isImgrUser"] boolValue]) {
                            
                            NSLog(@"added %@",[IMAppDelegate jidForPhoneNumber:[dict objectForKey: @"phonenumber"]]);
                            
                            NSString *jid = [dict objectForKey:@"jid"];
                            
                            if (jid.length)
                            {
                                NSString *newUserJid = [IMAppDelegate jidForPhoneNumber:jid];
                                if (YES == contact.is_imgr_user.boolValue)
                                {
                                    //IMGR contact already exist, hence
                                    if (NO == [newUserJid isEqualToString:contact.user_id])
                                    {
                                        //suspected buddy changed his device,
                                        oldJID = contact.user_id;
                                    }
                                }
                                
                                contact.user_id = newUserJid;
                                contact.member_id = [dict objectForKey: @"memberId"];
                                
                                NSLog(@"memID = %@",contact.member_id);
                                contact.is_imgr_user = [NSNumber numberWithBool: YES];
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [[CMChatManager sharedInstance] addBuddy:contact.user_id inHost:HOST_OPENFIRE withNickName:contact.first_name];
                                   });
                            }
                        }
                        else
                        {
                            if([contact.is_imgr_user boolValue])
                            {
                                contact.user_id = nil;
                                contact.member_id = nil;
                                contact.is_imgr_user = [NSNumber numberWithBool: NO];
                            }
                        }
                }

            dispatch_async(dispatch_get_main_queue(), ^{

                if([netManager.failDelegate respondsToSelector: @selector(netManager:didReceiveResponse:)])
                    [netManager.failDelegate netManager:netManager didReceiveResponse:dictionary];
            });
                [[NSUserDefaults standardUserDefaults] setBool: YES forKey: K_ALREADY_REGISTERED];

            } completionHandler:^{
                if (oldJID) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [[CMChatManager sharedInstance] deleteAllMessagesWithHost:HOST_OPENFIRE forJID:oldJID];
                    });
                }
            }];

        }break;
            
        case E_UPDATE_DEVICE_TOKEN_SERVICE:
            break;
            
        default:
            break;
    }
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
    
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    if([netManager.failDelegate respondsToSelector: @selector(netManager:didFailWithError:)])
        [netManager.failDelegate netManager:netManager didFailWithError:error];
//    else
//    {
//        UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
//        [registrationAlertView show];
//    }
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    if([netManager.failDelegate respondsToSelector: @selector(netManager:didFailToConnectWithServer:)])
        [netManager.failDelegate netManager:netManager didFailToConnectWithServer:error];
    else
    {
        [IMUtils noInternetAvailable];
    }
}

#pragma mark - TWTSideMenuViewControllerDelegate

- (UIStatusBarStyle)sideMenuViewController:(TWTSideMenuViewController *)sideMenuViewController statusBarStyleForViewController:(UIViewController *)viewController
{
    if (viewController == self.menuViewController) {
        return UIStatusBarStyleDefault;
    } else {
        return UIStatusBarStyleDefault;
    }
}

- (void)sideMenuViewControllerWillOpenMenu:(TWTSideMenuViewController *)sender {
    NSLog(@"willOpenMenu");
}

- (void)sideMenuViewControllerDidOpenMenu:(TWTSideMenuViewController *)sender {
    NSLog(@"didOpenMenu");
}

- (void)sideMenuViewControllerWillCloseMenu:(TWTSideMenuViewController *)sender {
    NSLog(@"willCloseMenu");
}

- (void)sideMenuViewControllerDidCloseMenu:(TWTSideMenuViewController *)sender {
	NSLog(@"didCloseMenu");
}

- (UIStoryboard *)settingsStoryboard
{
    if (nil == settingsStoryboard) {
        settingsStoryboard = [UIStoryboard storyboardWithName:@"SettingsControllers" bundle:nil];
    }
    return settingsStoryboard;
}

- (UIStoryboard *)contactsStoryboard
{
    if (nil == contactsStoryboard) {
        contactsStoryboard = [UIStoryboard storyboardWithName:@"IMContacts" bundle:nil];
    }
    return contactsStoryboard;
}

- (UIStoryboard *)messagesStoryboard;
{
    if (nil == messagesStoryboard) {
        messagesStoryboard = [UIStoryboard storyboardWithName:@"MessagesControllers" bundle:nil];
    }
    return messagesStoryboard;
}

- (UIStoryboard *)registrationStoryboard
{
    if (nil == registrationStoryboard) {
        registrationStoryboard = [UIStoryboard storyboardWithName:@"IMLogin" bundle:nil];
    }
    return registrationStoryboard;
}

+ (IMAppDelegate *)sharedDelegate
{
    return (IMAppDelegate *) [UIApplication sharedApplication].delegate;
}

- (void) login
{
    NSString *password = [[NSUserDefaults standardUserDefaults] valueForKey:k_DEFAULTS_KEY_USER_ID];
    if(password.length)
    {
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
        
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            
            
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }

        
        [[CMChatManager sharedInstance] connectAccountWithHost:HOST_OPENFIRE forJID:[IMAppDelegate jidForPhoneNumber:[self getUserID]] accessToken:password];
        if (NO == [[CMMessageArchivingCoreDataStorage sharedInstance]isStoreSet]) {
            [[CMMessageArchivingCoreDataStorage sharedInstance] executeBlock:^{
                DebugLog(@"executing the block to setup core data first time");
            }];
        }
    }
}

- (NSString*)getUserID
{
    NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:k_DEFAULTS_KEY_USER_ID];
    NSString* deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:K_DEFAULTS_DEVICE_UDID];
    NSAssert(nil != deviceToken, @"Device udid is not expected to be nil");
    userId = [NSString stringWithFormat:@"%@_%@",userId,deviceToken];
    return userId;
}

- (void)getPromos
{
    [self performSelector:@selector(getIMGRImages:) withObject:nil afterDelay:5];
    [self performSelector:@selector(getSponsoredAds) withObject:nil afterDelay:1];
    [self performSelector:@selector(getPersonalAds) withObject:nil afterDelay:3];
}

- (CMNetManager*)getIMGRImages: (id)target
{
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    
    NSString *jidString = [app getUserID];
    
    DebugLog(@"jidString = %@", jidString);
    
    [parameters setObject:jidString forKey: @"username"];
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kGetImgrImages] parameters:parameters queueIdentifier:"imgrImgaesQueue" queueType:SERIAL httpMethod: GET];
    netManager.delegate = self;
    netManager.failDelegate = target;
    netManager.tag = E_GET_PROMO_LIST_SERVICE;
    [netManager startRequest];
    return netManager;
}

- (void) downloadPromoImageForPromo:(IMPromos *)promo
{
    NSString* filePath = [[IMUtils getImageFolderPath] stringByAppendingPathComponent: [promo.promo_image lastPathComponent]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:filePath])
    {
        IMDownloadPictureOperation *pictOp = [[IMDownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleAspectFit picURL:[NSString stringWithFormat:@"%@%@", KBASE_IMAGE_URL, promo.promo_image]];
        pictOp.delegate = self;
        
        NSMutableDictionary *storedObject = [[NSMutableDictionary alloc] init];
        //[storedObject setObject:imageView forKey:@"imageView"];
        [storedObject setObject: promo forKey: @"model"];
        [storedObject setObject: promo.promo_image forKey: @"imageName"];
        [storedObject setObject:@"promo_image_path" forKey:@"imageKey"];
        [pictOp setStoredObject:storedObject];
        [queueOperation addOperation:pictOp];
    }
    else
    {
        if (2 != promo.promo_type.integerValue) {
            [[NSNotificationCenter defaultCenter] postNotificationName:PERSONAL_PROMO_THIRDPARTY_UPDATED_NOTIFICATION object:nil];
        }
    }
}

- (void)checkIMGRUsersFromServer
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        phoneNumberList = [[NSMutableString alloc] init];
        NSArray* allContacts = [[IMDataHandler sharedInstance] fetchAllContacts];
        for(IMContacts* contact in allContacts)
        {
            if(contact.phone_number_2) {
                [phoneNumberList appendString: contact.phone_number_2];
                [phoneNumberList appendString: @","];
            }
        }
        if([allContacts count] > 0)
            [self checkIMGRUsers:phoneNumberList delegate: nil];
    });
 
}

- (CMNetManager*)checkIMGRUsers: (NSMutableString*)formattedPhoneNumberList delegate: (id)delegate
{
    if(formattedPhoneNumberList.length == 0) {
        return nil;
    }
    DebugLog(@"checkIMGRUsers");
    NSMutableDictionary* parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:KAPIKEY forKey:@"apiKey"];
    [parameters setObject:formattedPhoneNumberList forKey: @"phonenumbers"];
    
    CMNetManager* netManager = [[CMNetManager alloc] initWithURL:[NSString stringWithFormat:@"%@%@", KBASEURL, kGetIMGRUsers] parameters:parameters queueIdentifier:"contactQueue" queueType:SERIAL httpMethod: POST];
    netManager.delegate = self;
    netManager.failDelegate = delegate;
    netManager.tag = E_CHECK_IMGR_SERVICE;
    [netManager startRequest];
    return netManager;
}

+ (NSString *)jidForPhoneNumber:(NSString *)phoneNumber
{
    return [NSString stringWithFormat:@"%@@%@",phoneNumber,HOST_OPENFIRE];
}

- (void)forHost:(NSString *)hostName didAuthenticateJID:(NSString *)JID
{
    NSString* token = [[NSUserDefaults standardUserDefaults] stringForKey: K_DEVICE_TOKEN];
    DebugLog(@"didAuthenticateJID Token = %@", token);
    [[CMChatManager sharedInstance] token:token registerPushForHost: HOST_OPENFIRE];
    
    [self performSelector:@selector(setUpUserProfile) withObject:nil afterDelay:2.5];
    [self performSelector:@selector(refreshThirdPartyPromos) withObject:nil afterDelay:1];
    [self checkIMGRUsersFromServer];
    [self setUpBlockingFeature];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMGR Success", @"IMGR Success") message:NSLocalizedString(@"Connected to IMGR.", @"Connected to IMGR.") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//    [alertView show];
}

-(void)didDownloadImage
{
    DebugLog(@"Promo image downloaded from the server");
    [[NSNotificationCenter defaultCenter] postNotificationName:PERSONAL_PROMO_THIRDPARTY_UPDATED_NOTIFICATION object:nil];
}
- (void)forHost:(NSString *)hostName didDisConnect:(NSString *)JID :(NSError *)error
{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMGR Fail", @"IMGR Fail") message:NSLocalizedString(@"Connection Disconnected with IMGR server.", @"Connection Disconnected with IMGR server.") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//    [alertView show];
    DebugLog(@"didDisConnect...\nRetrying login.......");
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {

    
        
    }
    else
    {
      [self performSelector:@selector(login) withObject:nil afterDelay:5.0];
    }
}

- (void)forHost:(NSString *)hostName willConnect:(NSString *)JID
{
    DebugLog(@"forHost:willConnect:,JID = %@",JID);

}
/**
 * @details This Delegate is called when timout has occured.
 **/
- (void)forHost:(NSString *)hostName didConnectionTimout:(NSString *)JID :(NSError*)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IMGR Fail", @"IMGR Fail") message:NSLocalizedString(@"Login fail with IMGR server timeout.", @"Login fail with IMGR server timeout.") delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alertView show];
}


- (void) setUpUserProfile
{
    NSDictionary *dictionary = [[NSUserDefaults standardUserDefaults] objectForKey:k_DEFAULTS_KEY_MY_VCARD];
    if (dictionary) {
        self.myVcard = dictionary;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *firstName = [_myVcard objectForKey:@"First_Name"];
            NSString *lastName = [_myVcard objectForKey:@"Last_Name"];
            
            NSString *emailID = [_myVcard objectForKey:@"Email_Id"];
            //UIImage *image =    [UIImage imageWithData:[_myVcard objectForKey:@"User_Image"]];
            NSData* imageData = [_myVcard objectForKey:@"User_Image"];
            
            [[CMChatManager sharedInstance] setupMyVcardInHost:HOST_OPENFIRE firstName:firstName lastName:lastName phoneNumber:nil emailID:emailID profilePhoto:imageData];
            //[[NSUserDefaults standardUserDefaults] removeObjectForKey:k_DEFAULTS_KEY_MY_VCARD];
            //[[NSUserDefaults standardUserDefaults] synchronize];
        });
    }
}

- (void)setUpBlockingFeature
{
    blockedJids = [NSMutableSet new];
    [self refreshBlockContacts];
    [[CMChatManager sharedInstance] setShouldStoreMessageblock:^BOOL(NSString *jid) {
        
        @synchronized(blockedJids)
        {
            if ([blockedJids containsObject:jid]) {
                return NO;
            }
            return YES;
        }
        
    } forHost:HOST_OPENFIRE];
}

- (void) refreshBlockContacts
{
    @synchronized(blockedJids)
    {
        [blockedJids removeAllObjects];
        for (IMContacts *contact in [self blockedContactFetched]) {
            if (contact.user_id)
                [blockedJids addObject:contact.user_id];
        }
    }
}

- (NSArray *)blockedContactFetched
{
    NSManagedObjectContext *moc = [[CMCoreDataHandler sharedInstance] managedObjectContext];
    if(!moc){
        return nil;
    }
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IMContacts"
                                              inManagedObjectContext:moc];
            NSSortDescriptor *sd2 = [[NSSortDescriptor alloc] initWithKey:@"first_name" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects: sd2, nil];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat: @"is_blocked == %d", YES];
    [fetchRequest setPredicate: predicate];
    
    return [moc executeFetchRequest:fetchRequest error:nil];
}

- (void) unBlockContact:(IMContacts *)contact
{
    contact.is_blocked = [NSNumber numberWithBool:NO];
    contact.is_imgr_user = [NSNumber numberWithBool:YES];
    [[IMDataHandler sharedInstance] saveDatabase];
    //SS as per kapil's comment we will not add/remove buddy on block/unlblock
//    [[CMChatManager sharedInstance] addBuddy:contact.user_id inHost:HOST_OPENFIRE withNickName:contact.first_name];
    [self refreshBlockContacts];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"The contact has been unblocked for you.", @"The contact has been unblocked for you.") delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @"Ok"), nil];
    [alertView show];
}

- (void) blockContact:(IMContacts *)contact
{
        //SS as per kapil's comment we will delete existing messages
//    [[CMChatManager sharedInstance] deleteAllMessagesWithHost:HOST_OPENFIRE forJID:contact.user_id];
    contact.is_blocked = [NSNumber numberWithBool:YES];
    contact.is_imgr_user = [NSNumber numberWithBool:NO];
    [[IMDataHandler sharedInstance] saveDatabase];
        //SS as per kapil's comment we will not add/remove buddy on block/unlblock
//    [[CMChatManager sharedInstance] removeBuddyInHost:HOST_OPENFIRE withJid:contact.user_id];
    [self refreshBlockContacts];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"CONTACT BLOCKED", @"CONTACT BLOCKED") delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @"Ok"), nil];
    [alertView show];
}


- (void)forHost:(NSString *)hostName forJID:(NSString*)JID didReceiveMessage:(CMMessageArchiving_Message_CoreDataObject*)message
{
    
    NSLog(@"didReceiveMessage from AppDelegate = %@", message.message);
    NSXMLElement* element = [message.message elementForName: @"delay"];
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground && !element) {
        
        if(![[[NSUserDefaults standardUserDefaults] valueForKey: k_DEFAULTS_KEY_NOTIFICATIONS] boolValue])
            return;
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        
        
        //localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60];
        NSData *jsonData = [message.message.body dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error = nil;
        
        NSDictionary *jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        NSLog(@"%@", jsonObjects);
        
        NSString* bodyStr = [jsonObjects valueForKey: MESSAGE_BODY_TAG];
        if(bodyStr.length)
            localNotification.alertBody = bodyStr;
        
        XMPPJID* jid = [message.message from].bareJID;
        
        NSString* userJid = [IMAppDelegate jidForPhoneNumber: [IMUtils phoneNumberForJid:jid.user]];
        NSLog(@"sender JID = %@", userJid);
        
        NSString* messageStr = [[jsonObjects valueForKey: MESSAGE_BODY_TAG] emoticonizedString];
        IMContacts* contact = [[IMDataHandler sharedInstance] fetchContactWithUserJid: userJid];
        if(contact.first_name.length) {
            localNotification.alertBody = [NSString stringWithFormat: @"%@\n%@", contact.first_name, messageStr];
        }
        else {
            NSRange range = [[IMUtils phoneNumberForJid:jid.user] rangeOfString: @"_"];
            NSString* phoneNo = [[IMUtils phoneNumberForJid:jid.user] substringToIndex: range.location];
            localNotification.alertBody = [NSString stringWithFormat: @"%@\n%@", phoneNo, messageStr];
        }
        
        NSMutableDictionary* infoDict = [[NSMutableDictionary alloc] init];
        [infoDict setObject:userJid forKey: @"sender_id"];
        
        localNotification.userInfo = infoDict;
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}





- (void) refreshThirdPartyPromos
{
    NSArray *thirdPartyPromos = [[IMDataHandler sharedInstance] fetchThirdPartyPersonalPromos];
    for (IMPromos *promo in thirdPartyPromos) {
        [self downloadPromoWithId:promo.promo_id.integerValue];
    }
}









@end
