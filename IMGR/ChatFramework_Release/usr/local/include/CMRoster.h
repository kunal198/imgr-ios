//
//  CMRoster.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 04/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import "CMChatFrameworkConstants.h"
#import "XMPPFramework.h"
#import "CMRosterCoreDataStorage.h"

@class CMStream;
@interface CMRoster : XMPPRoster 
{
    
}
@property (nonatomic, weak) id<CMRosterCoreDataStorageDelegate> rosterCoreDataStorageDelegate;
- (CMRoster *)initWithStream:(CMStream *)stream;

@end
