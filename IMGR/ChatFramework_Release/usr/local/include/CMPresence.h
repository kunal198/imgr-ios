//
//  CMPresence.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 16/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import "XMPPPresence.h"

@class CMStream;
@interface CMPresence : XMPPPresence

-(CMPresence *)initWithStream:(CMStream *)stream;
@end

