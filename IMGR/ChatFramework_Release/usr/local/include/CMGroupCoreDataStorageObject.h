//
//  XMPPGroupCoreDataStorageObject.h
//  iPhoneXMPP
//
//  Created by Saurabh Verma on 20/12/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CMUserCoreDataStorageObject;
@class CMUserGoogleCoreDataStorageObject;

@interface CMGroupCoreDataStorageObject : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *users;

+ (void)clearEmptyGroupsInManagedObjectContext:(NSManagedObjectContext *)moc;

+ (id)fetchOrInsertGroupName:(NSString *)groupName inManagedObjectContext:(NSManagedObjectContext *)moc;

+ (id)insertGroupName:(NSString *)groupName inManagedObjectContext:(NSManagedObjectContext *)moc;

@end

@interface CMGroupCoreDataStorageObject (CoreDataGeneratedAccessors)

- (void)addUsersObject:(CMUserCoreDataStorageObject *)value;
- (void)removeUsersObject:(CMUserCoreDataStorageObject *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

- (void)addGoogleUsersObject:(CMUserCoreDataStorageObject *)value;
- (void)removeGoogleUsersObject:(CMUserCoreDataStorageObject *)value;
- (void)addGoogleUsers:(NSSet *)values;
- (void)removeGoogleUsers:(NSSet *)values;

@end
