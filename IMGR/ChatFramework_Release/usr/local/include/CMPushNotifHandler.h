//
//  CMPushNotifHandler.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 03/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"

@interface CMPushNotifHandler : NSObject 

+ (CMPushNotifHandler*)sharedInstance;

- (void)token:(NSString *)deviceToken registerPushForStream:(XMPPStream *)stream;
@end
