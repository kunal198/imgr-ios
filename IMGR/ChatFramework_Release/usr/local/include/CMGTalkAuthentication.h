//
//  CMGTalkAuthentication.h
//  iPhoneXMPP
//
//  Created by Saurabh Verma on 23/12/11.
//
//

#import <Foundation/Foundation.h>
#import "XMPPSASLAuthentication.h"
#import "XMPPStream.h"

@interface CMGTalkAuthentication : NSObject <XMPPSASLAuthentication>


@end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface XMPPStream (XMPPGoogleAuthentication)

- (BOOL)supportsGoogleAuthAuthentication;

- (BOOL)authenticateWithGoogleAccessToken:(NSString *)accessToken error:(NSError **)errPtr;
@end