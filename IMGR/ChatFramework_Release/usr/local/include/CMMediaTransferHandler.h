//
//  CMMediaTransfer.h
//  CMChatFramework
//
//  Created by Tarun Khurana on 08/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMMediaTransferHandlerDelegate;

@interface CMMediaTransferHandler : NSObject
{
    __weak id <CMMediaTransferHandlerDelegate> delegate;
}
+ (CMMediaTransferHandler*)sharedInstance;
@property (nonatomic,weak)id <CMMediaTransferHandlerDelegate> delegate;

- (void)media:(NSData *)data ofType:(NSString *)mediaType withTag:(NSInteger) tag transferMediaForStream:(XMPPStream *)stream toJID:(NSString *)toJIDs;
- (void)media:(NSData *)data ofType:(NSString *)mediaType withTag:(NSInteger) tag transferMediaForStream:(XMPPStream *)stream toJID:(NSString *)toJID previewImage:(UIImage *)previewImage;

@end
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMMediaTransferHandlerDelegate <NSObject>

@required
- (void)stream:(XMPPStream *)sender didProgressTransferMediaPercent:(float)percent withTag:(NSInteger)tag forJID:(NSString *)toJID;
- (void)stream:(XMPPStream *)sender didCompleteTransferMedia:(NSString *)response withTag:(NSInteger)tag forJID:(NSString *)toJID;
- (void)stream:(XMPPStream *)sender didCompleteTransferMedia:(NSString *)response withTag:(NSInteger)tag forJID:(NSString *)toJID previewImage:(UIImage *)image;
- (void)stream:(XMPPStream *)sender didFailTransferMedia:(NSError *)error withTag:(NSInteger)tag forJID:(NSString *)toJID;
@optional

@end
