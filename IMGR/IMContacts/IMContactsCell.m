//
//  IMContactsCell.m
//  IMGR
//
//  Created by akram on 10/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMContactsCell.h"

@implementation IMContactsCell
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:[IMContactsCell cellReusableIdentifier]];
    
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (IMContactsCell *)cellLoadedFromNibFile
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    for (NSObject *obj in objects)
    {
        if ([obj isKindOfClass:[self class]])
        {
            return (IMContactsCell *)obj;
            break;
        }
    }
    return nil;
}

+ (NSString *) cellReusableIdentifier
{
    return @"IMContactsTableCell";
}

- (IBAction)onStatusButtonClick:(id)sender
{
    if([delegate respondsToSelector: @selector(onStatusButtonClick:)])
        [delegate onStatusButtonClick: sender];
}

- (IBAction)onInfoButtonClick:(id)sender
{
    if([delegate respondsToSelector: @selector(onInfoButtonClick:)])
        [delegate onInfoButtonClick: sender];
}
@end
