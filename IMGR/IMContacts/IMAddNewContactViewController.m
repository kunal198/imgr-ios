//
//  IMAddNewContactViewController.m
//  IMGR
//
//  Created by akram on 07/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMAddNewContactViewController.h"
#import "IMNewContactCell.h"
#import "IMNonIMGRContactDetailViewController.h"
#import "CMCoreDataHandler.h"
#import "IMContacts.h"
#import "IMDataHandler.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "IMUtils.h"
#import "MBProgressHUD.h"
#import "IMChatViewController.h"

enum
{
    E_FIRST_NAME_FIELD_TAG        = 0,
    E_LAST_NAME_FIELD_TAG,
    E_PHONE_NUMBER_FIELD_TAG,
    E_EMAIL_FIELD_TAG,
};

@interface IMAddNewContactViewController ()
{
    MBProgressHUD* progressView;
}
@end

@implementation IMAddNewContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    progressView = nil;//SS we do not need it
    progressView = [[MBProgressHUD alloc] initWithView: self.view];
    progressView.labelText = @"Please wait...";
    
    [app.window addSubview: progressView];
    
    labelPhone = [[UILabel alloc]initWithFrame:CGRectMake(0, 400, 320, 50)];
    labelPhone.numberOfLines = 2;
    labelPhone.font = [UIFont systemFontOfSize:13];
    labelPhone.textAlignment = NSTextAlignmentCenter;
    labelPhone.text =@"Phone Numbers Are 10 Digits Only, And Do Not Include A Country Code.";
    [self.view addSubview:labelPhone];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction methods
- (void)addContact
{
    ABRecordRef person = ABPersonCreate();
    
    IMNewContactCell* firstname = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
    
    IMNewContactCell* lastname = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:1 inSection:0]];
    
    IMNewContactCell* phoneNumber = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:2 inSection:0]];
    
    IMNewContactCell* email = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:3 inSection:0]];
    if (firstname.editTextField.text.length) {
        ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFStringRef) [firstname.editTextField.text capitalizedString], NULL);
    }
    
    if (lastname.editTextField.text.length) {
        ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFStringRef) [lastname.editTextField.text capitalizedString], NULL);
    }
    
    CFErrorRef error = nil;
    if (email.editTextField.text.length) {
        ABMutableMultiValueRef emailMultiValue = ABMultiValueCreateMutable(kABPersonEmailProperty);
        bool didAddEmail = ABMultiValueAddValueAndLabel(emailMultiValue, CFRetain((__bridge CFTypeRef)(email.editTextField.text)), kABOtherLabel, NULL);
        
        if(didAddEmail){
            ABRecordSetValue(person, kABPersonEmailProperty, emailMultiValue,  NULL);
        } else {
            NSLog(@"Error adding email: %@", error);
            error = nil;
        }
    }
    NSString *phoneNumberText = phoneNumber.editTextField.text ;
    
    ABMutableMultiValueRef phoneMultiValue = ABMultiValueCreateMutable(kABPersonPhoneProperty);
    bool didAddPhone = ABMultiValueAddValueAndLabel(phoneMultiValue, CFRetain((__bridge CFTypeRef)(phoneNumberText)), kABOtherLabel, NULL);
    
    if(didAddPhone){
        ABRecordSetValue(person, kABPersonPhoneProperty, phoneMultiValue,  NULL);
    } else {
        NSLog(@"Error adding phone: %@", error);
        error = nil;
    }
    
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    //Add person Object to addressbook Object.
    ABAddressBookAddRecord(addressBookRef, person, NULL);
    

    if (ABAddressBookSave(addressBookRef, nil)) {
        
        ABRecordID contactID = ABRecordGetRecordID(person);

        IMContacts* contact = nil;
        
        contact = [[IMDataHandler sharedInstance] fetchContactWithPhoneNumber: [IMUtils phonenumberFromFormattedNumber: phoneNumber.editTextField.text]];
        if(contact == nil)
            contact = [NSEntityDescription insertNewObjectForEntityForName: @"IMContacts" inManagedObjectContext: [[CMCoreDataHandler sharedInstance] managedObjectContext]];
        else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message:@"Number already exist" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            return;
        }
        contact.contact_id      = [NSString stringWithFormat: @"%d", contactID];
        contact.first_name      = firstname.editTextField.text;
        contact.last_name       = lastname.editTextField.text;
        contact.phone_number    = phoneNumber.editTextField.text;
        contact.email           = email.editTextField.text;
        contact.phone_number_2  = [IMUtils phonenumberFromFormattedNumber: phoneNumber.editTextField.text];
        
        NSString *firstName = [contact.first_name lowercaseString];
        if([firstName length] > 0)
            contact.index_character = [firstName substringToIndex: 1];
        
        BOOL isSaved = [[IMDataHandler sharedInstance] saveDatabase];
        if(isSaved)
        {
            DebugLog(@"\nPerson Saved successfuly");
            if(!self.imgrPhoneNumber)
            {
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Contact saved successfully" delegate:self cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
                [alertView show];
            }
            else
            {
                [progressView show: YES];
                [app checkIMGRUsers: (NSMutableString *)self.imgrPhoneNumber delegate: self];
            
            }
        }
        
    } else {
        DebugLog(@"\n Error Saving person to AddressBook");
    }
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (IBAction)onDoneButtonClick:(id)sender
{
    self.navigationItem.rightBarButtonItem.enabled = NO;
   
    
    IMNewContactCell* email = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:3 inSection:0]];
    if([[email.editTextField text]length])
    {
        if (![self validEmail:[email.editTextField text]])
        {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Please enter valid email address" delegate:nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
            [alertView show];
            [email.editTextField becomeFirstResponder];
            self.navigationItem.rightBarButtonItem.enabled = YES;
            return;
        }

    }
    
    IMNewContactCell* phoneNumber = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:2 inSection:0]];
    
    if(![phoneNumber.editTextField.text length])
    {
        
         UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Please enter valid phone number" delegate:nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
         [alertView show];
            self.navigationItem.rightBarButtonItem.enabled = YES;
         return;
        
    }
//    else if([phoneNumber.editTextField.text length]<14 || [phoneNumber.editTextField.text length]>16)
//    {
//        NSLog(@"phno=%@ length=%d",phoneNumber.editTextField.text,phoneNumber.editTextField.text.length);
//        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: @"Please enter valid phone number" delegate:nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
//        [alertView show];
//        phoneNumber.editTextField.text=@"";
//        self.navigationItem.rightBarButtonItem.enabled = YES;
//        return;
//
//    }
    [self addContact];
}

#pragma mark - UIAlertView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated: NO];
    
    IMNewContactCell* phoneNumber = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:2 inSection:0]];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"ADD_NEW_CONTACT" object: phoneNumber.editTextField.text];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IMNonIMGRContactDetailViewController* controller = segue.destinationViewController;
    
    controller.navigationItem.backBarButtonItem.title = @"Contacts";
    IMNewContactCell* phoneNumber = (IMNewContactCell*)[newContactTableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:2 inSection:0]];
    
    IMContacts* contact = [[IMDataHandler sharedInstance] fetchContactWithPhoneNumber: [IMUtils phonenumberFromFormattedNumber: phoneNumber.editTextField.text]];
    controller.contact = contact;
}
#pragma mark - UITableView Delegate/DataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier = @"identifier";
    
    IMNewContactCell* cell = (IMNewContactCell*)[tableView dequeueReusableCellWithIdentifier: identifier];
    
    if(cell == nil)
    {
        cell = [IMNewContactCell cellLoadedFromNibFile];
    }
    cell.editTextField.delegate = self;
    cell.editTextField.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.delegate = self;
    switch (indexPath.row)
    {
        case 0:
            cell.contactNameLabel.text = NSLocalizedString(@"FIRST NAME",@"");
            cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
            cell.editTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            break;
            
        case 1:
            cell.contactNameLabel.text = NSLocalizedString(@"LAST NAME",@"");
            cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
            cell.editTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;

            break;
            
        case 2:
            cell.contactNameLabel.text = NSLocalizedString(@"PHONE NUMBER", @"");
            if(self.imgrPhoneNumber){
                cell.editTextField.text = self.imgrPhoneNumber;
                cell.editTextField.enabled = NO;
            }
            else
                cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
            cell.editTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            break;
            
        case 3:
            cell.contactNameLabel.text = NSLocalizedString(@"EMAIL", @"");
            cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
            cell.editTextField.keyboardType = UIKeyboardTypeEmailAddress;
            break;
        case 4:
            
            cell.contactNameLabel.text = NSLocalizedString(@"Phone Numbers Are 10 Digits Only, And Do Not Include A Country Code.", @"");
           // cell.editTextField.placeholder = NSLocalizedString(@"TAP TO ENTER", @"");
           // cell.editTextField.keyboardType = UIKeyboardTypeEmailAddress;
            break;
        default:
            break;
    }
    
    
    return cell;
}



/*- (BOOL)validateEmail:(NSString *)emailStr
{
    if (emailStr.length == 0) {
        return YES;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}*/

-(BOOL) validEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    int charCount = [newString length];
    
    if([string isEqualToString:@" "] && charCount == 1) {
        return NO;
    }
    
    if(textField.tag == E_PHONE_NUMBER_FIELD_TAG)
    {
        if(textField.tag == 1000)
        {
            NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789+"];
            if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
                || [string rangeOfString:@"+"].location != NSNotFound
                || charCount > 4) {
                return NO;
            }
            
            if (![string isEqualToString:@""])
            {
                if (charCount == 1)
                {
                    newString = [NSString stringWithFormat:@"+%@", newString];
                }
            }
            textField.text = newString;
            textField.text = [textField.text substringToIndex: [textField.text length] - 1];
            return YES;
        }
        
        NSCharacterSet *numSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789-() "];
        if ([newString rangeOfCharacterFromSet:[numSet invertedSet]].location != NSNotFound
            || [string rangeOfString:@")"].location != NSNotFound
            || [string rangeOfString:@"("].location != NSNotFound
            || [string rangeOfString:@"-"].location != NSNotFound
            || charCount > MAX_PHONE_NUMBER) {
            
            return NO;
        }
        if (![string isEqualToString:@""])
        {
            if (charCount == 1)
            {
                newString = [NSString stringWithFormat:@"(%@", newString];
            }
            else if(charCount == 4)
            {
                newString = [newString stringByAppendingString:@") "];
            }
            else if(charCount == 5)
            {
                newString = [NSString stringWithFormat:@"%@) %@", [newString substringToIndex:4], [newString substringFromIndex:4]];
            }
            else if(charCount == 6)
            {
                newString = [NSString stringWithFormat:@"%@ %@", [newString substringToIndex:5], [newString substringFromIndex:5]];
            }
            
            else if (charCount == 9)
            {
                newString = [newString stringByAppendingString:@"-"];
            }
            else if(charCount == 10)
            {
                newString = [NSString stringWithFormat:@"%@-%@", [newString substringToIndex:9], [newString substringFromIndex:9]];
            }
        }
        textField.text = newString;
        return NO;
    }
    else
        return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    UITextField* tField = (UITextField*)[newContactTableView viewWithTag: textField.tag + 1];
    [tField becomeFirstResponder];
    
    return YES;
}

#pragma mark - CMNetManager Delegate methods
//This delegate gets called on success
-(void)netManager:(CMNetManager*)netManager didReceiveResponse:(NSDictionary*)dictionary
{
    [progressView hide: YES];
    [self.chatVC performSelector:@selector(refreshViewForSwitchChat) withObject:nil afterDelay:1];
    NSLog(@"Response = %@", dictionary);
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Contact saved successfully" delegate:self cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
    [alertView show];
}

-(void)netManager:(CMNetManager*)netManager didReceiveResponseString:(NSString*)response
{
    
}

//This delegate gets called on failure
-(void)netManager:(CMNetManager*)netManager didFailWithError:(NSError*)error
{
    [progressView hide: YES];
//    UIAlertView* registrationAlertView = [[UIAlertView alloc] initWithTitle: @"" message:error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles: @"OK", nil];
//    [registrationAlertView show];
}

-(void)netManager:(CMNetManager *)netManager didGetSuccessToConnectWithServer:(NSError *)error
{
    
}

//This delegate gets called if internet connection is not available
-(void)netManager:(CMNetManager *)netManager didFailToConnectWithServer:(NSError *)error
{
    [progressView hide: YES];
    [IMUtils noInternetAvailable];
}
@end
