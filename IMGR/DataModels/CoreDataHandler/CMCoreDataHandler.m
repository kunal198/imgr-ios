//
//  DFCoreDataHandler.m
//  Laser Liaison
//
//  Created by Akram on 4/4/13.

#import "CMCoreDataHandler.h"

@interface CMCoreDataHandler()
{
    NSManagedObjectContext *backgroundContext;
}
@end

@implementation CMCoreDataHandler

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext;
@synthesize persistentStoreCoordinator;
@synthesize managedObjectModel;

static CMCoreDataHandler *sharedInstance;

#pragma mark - Alloc Singleton Class Object

+ (CMCoreDataHandler *)sharedInstance
{
    @synchronized(self)
    {
        if(!sharedInstance)
        {
            sharedInstance = [[CMCoreDataHandler alloc] init];
        }
    }
    return sharedInstance;
}

+(id)alloc 
{
    @synchronized(self) 
    {
        NSAssert(sharedInstance == nil, @"Attempted to allocate a second instance of a singleton FAWDataController.");
        sharedInstance = [super alloc];
    }
    
    return sharedInstance;
}

#pragma mark - Core Data Store Objects Init

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext1 = self.managedObjectContext;
    if (managedObjectContext1 != nil)
    {
        if ([managedObjectContext1 hasChanges] && ![managedObjectContext1 save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSString *)applicationDocumentsDirectory 
{
	return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)mergeChangesFrom_iCloud:(NSNotification *)notification {
    
	NSLog(@"Merging in changes from iCloud...");
    
    NSManagedObjectContext* moc = [self managedObjectContext];
    
    [moc performBlock:^{
        
        [moc mergeChangesFromContextDidSaveNotification:notification];
        
        NSNotification* refreshNotification = [NSNotification notificationWithName:@"SomethingChanged"
                                                                            object:self
                                                                          userInfo:[notification userInfo]];
        
        [[NSNotificationCenter defaultCenter] postNotification:refreshNotification];
    }];
}

- (NSManagedObjectModel *)managedObjectModel 
{
    if (managedObjectModel != nil)
    {
        return managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"IMCoreDataModel" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL: modelURL];
    
    return managedObjectModel;
}

-(void)copyDatabaseIfNeeded{
    BOOL success;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    NSString *finalPath = [documentPath stringByAppendingPathComponent:@"IMCoreDataModel.sqlite"];
    
    success = [fileManager fileExistsAtPath:finalPath];
    
    if(success)
    {
        NSLog(@"Database Already Created.");
        return;
    }
    
    NSString *defaultPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"IMCoreDataModel.sqlite"];
    
    success = [fileManager copyItemAtPath:defaultPath toPath:finalPath error:&error];
    
    if(success)
    {
        NSLog(@"Database Created Successfully.");
    }
    
}

- (NSURL *)applicationDocumentDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSString *)persistentStoreDirectory
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : NSTemporaryDirectory();
	
	// Attempt to find a name for this application
	NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
	if (appName == nil) {
		appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
	}
	
	if (appName == nil) {
		appName = @"xmppframework";
	}
	
	
	NSString *result = [basePath stringByAppendingPathComponent:appName];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	if (![fileManager fileExistsAtPath:result])
	{
		[fileManager createDirectoryAtPath:result withIntermediateDirectories:YES attributes:nil error:nil];
	}
	
    return result;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if(persistentStoreCoordinator != nil)
    {
        return persistentStoreCoordinator;
    }
    else
    {
        NSString *docsPath = [self persistentStoreDirectory];
        NSString *storePath = [docsPath stringByAppendingPathComponent: @"IMCoreDataModel.sqlite"];
        
        //NSURL  *storeUrl = [[self applicationDocumentDirectory] URLByAppendingPathComponent: ];
        NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
        NSError *error = nil;
        
        persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
       // NSPersistentStoreCoordinator *psc = persistentStoreCoordinator;
        // Set up iCloud in another thread:
        
        /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            // ** Note: if you adapt this code for your own use, you MUST change this variable:
            NSString *iCloudEnabledAppID = @"imgrinhouse";
            
            // ** Note: if you adapt this code for your own use, you should change this variable:
            NSString *dataFileName = @"IMCoreDataModel.sqlite";
            
            // ** Note: For basic usage you shouldn't need to change anything else
            
            NSString *iCloudDataDirectoryName = @"Data.nosync";
            NSString *iCloudLogsDirectoryName = @"Logs";
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *docsPath = [self persistentStoreDirectory];
            NSString *storePath = [docsPath stringByAppendingPathComponent: @"IMCoreDataModel.sqlite"];
            
            //NSURL  *storeUrl = [[self applicationDocumentDirectory] URLByAppendingPathComponent: ];
            NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
            //NSURL *localStore = [[self applicationDocumentDirectory] URLByAppendingPathComponent:dataFileName];
            NSURL *iCloud = [fileManager URLForUbiquityContainerIdentifier: nil];
            
            [psc lock];
            if (iCloud) {
                
                NSLog(@"iCloud is working");
                
                NSURL *iCloudLogsPath = [NSURL fileURLWithPath:[[iCloud path] stringByAppendingPathComponent:iCloudLogsDirectoryName]];
                
                NSLog(@"iCloudEnabledAppID = %@",iCloudEnabledAppID);
                NSLog(@"dataFileName = %@", dataFileName);
                NSLog(@"iCloudDataDirectoryName = %@", iCloudDataDirectoryName);
                NSLog(@"iCloudLogsDirectoryName = %@", iCloudLogsDirectoryName);
                NSLog(@"iCloud = %@", iCloud);
                NSLog(@"iCloudLogsPath = %@", iCloudLogsPath);
                
                if([fileManager fileExistsAtPath:[[iCloud path] stringByAppendingPathComponent:iCloudDataDirectoryName]] == NO) {
                    DebugLog(@"File Does not exist");
                    NSError *fileSystemError;
                    [fileManager createDirectoryAtPath:[[iCloud path] stringByAppendingPathComponent:iCloudDataDirectoryName]
                           withIntermediateDirectories:YES
                                            attributes:nil
                                                 error:&fileSystemError];
                    if(fileSystemError != nil) {
                        NSLog(@"Error creating database directory %@", fileSystemError);
                    }
                }
                
                NSString *iCloudData = [[[iCloud path]
                                         stringByAppendingPathComponent:iCloudDataDirectoryName]
                                        stringByAppendingPathComponent:dataFileName];
                
                NSLog(@"iCloudData = %@", iCloudData);
                
                NSMutableDictionary *options = [NSMutableDictionary dictionary];
                [options setObject:[NSNumber numberWithBool:YES] forKey:NSMigratePersistentStoresAutomaticallyOption];
                [options setObject:[NSNumber numberWithBool:YES] forKey:NSInferMappingModelAutomaticallyOption];
                [options setObject:iCloudEnabledAppID            forKey:NSPersistentStoreUbiquitousContentNameKey];
                [options setObject:iCloudLogsPath                forKey:NSPersistentStoreUbiquitousContentURLKey];
                
                
                
                [psc addPersistentStoreWithType:NSSQLiteStoreType
                                  configuration:nil
                                            URL:[NSURL fileURLWithPath:iCloudData]
                                        options:options
                                          error:nil];
                
                //[psc unlock];
            }
            else {
                NSLog(@"iCloud is NOT working - using a local store");
                NSMutableDictionary *options = [NSMutableDictionary dictionary];
                [options setObject:[NSNumber numberWithBool:YES] forKey:NSMigratePersistentStoresAutomaticallyOption];
                [options setObject:[NSNumber numberWithBool:YES] forKey:NSInferMappingModelAutomaticallyOption];
                
                //[psc lock];
                
                [psc addPersistentStoreWithType:NSSQLiteStoreType
                                  configuration:nil
                                            URL:storeUrl
                                        options:options
                                          error:nil];
                //[psc unlock];
                
            }
            
            [psc unlock];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SomethingChanged" object:self userInfo:nil];
            });
        });*/
        
        if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) 
        {
            [[NSFileManager defaultManager] removeItemAtPath:storeUrl.path error:nil];
            return [self persistentStoreCoordinator];
        }    
        
        return persistentStoreCoordinator;
    }
}

-(NSManagedObjectContext*)managedObjectContext
{
    if(managedObjectContext != nil)
    {
        return managedObjectContext;
    }
    else
    {
        NSPersistentStoreCoordinator* coordinator = [self persistentStoreCoordinator];
        if(coordinator != nil)
        {
            managedObjectContext = [[NSManagedObjectContext alloc]initWithConcurrencyType: NSMainQueueConcurrencyType];
            
            [managedObjectContext setPersistentStoreCoordinator: coordinator];
            
            [managedObjectContext performBlockAndWait:^{
            
                [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(mergeChangesFrom_iCloud:) name:NSPersistentStoreDidImportUbiquitousContentChangesNotification object:coordinator];

            }];
            //__managedObjectContext = managedObjectContext;
            
            //[managedObjectContext setPersistentStoreCoordinator:coordinator];
        }
         backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        backgroundContext.parentContext = managedObjectContext;
        return managedObjectContext;
    }
}
- (NSManagedObjectContext *)backgroundContext
{
    return backgroundContext;
}

- (void) performAndSaveDataBaseTaskInBackground:(BackgroundTasks) lengthyTasks completionHandler:(BackgroundTasks) performOnSaveData
{
    @synchronized(self)
    {
        [backgroundContext performBlock:^{
            lengthyTasks();
            NSError *error;
            if (![backgroundContext save:&error]){
                NSLog(@"Error observed in save background context: %@",[error localizedDescription]);
            }
            else{
                [managedObjectContext performBlock:^{
                    NSError *error;
                    if (![managedObjectContext save:&error]){
                        NSLog(@"Error observed in save main context: %@",[error localizedDescription]);
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        performOnSaveData(); 
                    });
                }];
            }
        }];
    }
}

- (void) performAndSaveDataBaseTaskInBackground:(BackgroundTasks) lengthyTasks
{
    @synchronized(self)
    {
        [backgroundContext performBlock:^{
            lengthyTasks();
            NSError *error;
            if (![backgroundContext save:&error]){
                NSLog(@"Error observed in save background context: %@",[error localizedDescription]);
            }
            else{
                [managedObjectContext performBlock:^{
                    NSError *error;
                    if (![managedObjectContext save:&error]){
                        NSLog(@"Error observed in save main context: %@",[error localizedDescription]);
                    }
                }];
            }
        }];
    }
}

@end
