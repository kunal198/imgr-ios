//
//  IMPromos.m
//  IMGR
//
//  Created by akram on 19/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import "IMPromos.h"


@implementation IMPromos

@dynamic is_enabled;
@dynamic promo_creation_date;
@dynamic promo_id;
@dynamic promo_image;
@dynamic promo_link;
@dynamic promo_link_text;
@dynamic promo_message;
@dynamic promo_name;
@dynamic promo_type;
@dynamic status;
@dynamic index_character;
@dynamic promo_image_path;
@dynamic markedAsDeleted;
@end
