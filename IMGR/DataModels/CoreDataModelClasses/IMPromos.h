//
//  IMPromos.h
//  IMGR
//
//  Created by akram on 19/02/14.
//  Copyright (c) 2014 Copper Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IMPromos : NSManagedObject

@property (nonatomic, retain) NSNumber * is_enabled;
@property (nonatomic, retain) NSDate * promo_creation_date;
@property (nonatomic, retain) NSNumber * promo_id;
@property (nonatomic, retain) NSString * promo_image;
@property (nonatomic, retain) NSString * promo_link;
@property (nonatomic, retain) NSString * promo_link_text;
@property (nonatomic, retain) NSString * promo_message;
@property (nonatomic, retain) NSString * promo_name;
@property (nonatomic, retain) NSNumber * promo_type;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * index_character;
@property (nonatomic, retain) NSString * promo_image_path;
@property (nonatomic, retain) NSNumber * markedAsDeleted;
@end
