//
//  CMMessageArchivingCoreDataStorage.h
//  CMChatFramework
//
//  Created by Saurabh Verma on 15/01/11.
//  Copyright (c) 2014 Copper Mobile India Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"
#import "XMPPMessageArchivingCoreDataStorage.h"
#import "XMPPCoreDataStorage.h"
#import "CMRoomMessageCoreDataStorageObject.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMMessageArchivingCoreDataStorageDelegate;
@class CMMessageArchiving_Contact_CoreDataObject;
@class CMMessageArchiving_Message_CoreDataObject;
@class CMMessageArchiving;
@class CMStream;
@interface CMMessageArchivingCoreDataStorage : XMPPCoreDataStorage <XMPPMessageArchivingStorage>
{
    
}
@property (nonatomic, strong) id<CMMessageArchivingCoreDataStorageDelegate> delegate;
@property (strong) NSString *messageEntityName;
@property (strong) NSString *contactEntityName;
@property (assign) BOOL isStoreSet;

- (NSEntityDescription *)messageEntity:(NSManagedObjectContext *)moc;
- (NSEntityDescription *)contactEntity:(NSManagedObjectContext *)moc;

+ (CMMessageArchivingCoreDataStorage *)sharedInstance;
- (void)addParentObject:(CMMessageArchiving*)messageArchiving;
- (void)removeParentObject:(CMMessageArchiving*)messageArchiving;

- (void)activateFetchedResultsController;

- (void)archiveFailedMessage:(XMPPMessage *)message outgoing:(BOOL)isOutgoing xmppStream:(XMPPStream *)xmppStream;

- (void)deleteAllMessagesForContact:(NSString *)bareJidStr withHostName:(NSString*)hostName;

- (void)deleteArchivedMessages:(NSArray *)messages;

- (void)resetUnreadCount:(NSString*)jid;
- (NSUInteger)unreadCount:(NSString*)jid;
- (void)updateUnreadCount:(NSString*)jid;
- (void)createContactWithRoomJid:(NSString*)jid stream:(CMStream*)stream;
- (void)deleteContactWithJid:(NSString *)jid andHostName:(NSString *)hostName;
- (void)updatedContactWithJid:(NSString *)jid forMostRecentRoomMessage:(CMRoomMessageCoreDataStorageObject *)roommessage;
- (NSArray*)getMessagesforJID:(NSString*)JID limit:(NSInteger)limit host:(NSString*)hostName myJid:(NSString*)myJid;

// Overrides
- (CMMessageArchiving_Contact_CoreDataObject *)contactForMessage:(CMMessageArchiving_Message_CoreDataObject *)msg;

- (CMMessageArchiving_Contact_CoreDataObject *)contactWithJid:(XMPPJID *)contactJid
                                                      streamJid:(XMPPJID *)streamJid
                                           managedObjectContext:(NSManagedObjectContext *)moc;

- (CMMessageArchiving_Contact_CoreDataObject *)contactWithBareJidStr:(NSString *)contactBareJidStr
                                                      streamBareJidStr:(NSString *)streamBareJidStr
                                                  managedObjectContext:(NSManagedObjectContext *)moc;
- (void)forceRefreshActiveChatMessages;
@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Declaring a Protocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
@protocol CMMessageArchivingCoreDataStorageDelegate <NSObject>

@required
- (void)messageArchivingCoreDataStorage:(CMMessageArchivingCoreDataStorage*)messageStorage didUpdateMessages:(NSArray*)messages;

- (void)messageArchivingCoreDataStorage:(CMMessageArchivingCoreDataStorage*)messageStorage didRecieveMessage:(CMMessageArchiving_Message_CoreDataObject*)message withStream:(XMPPStream*)stream;

- (void)messageArchivingCoreDataStorage:(CMMessageArchivingCoreDataStorage*)messageStorage didSendMessage:(CMMessageArchiving_Message_CoreDataObject*)message withStream:(XMPPStream*)stream;

- (void)messageArchivingCoreDataStorage:(CMMessageArchivingCoreDataStorage*)messageStorage didFailToSendMessage:(CMMessageArchiving_Message_CoreDataObject*)message withStream:(XMPPStream*)stream;

- (void)messageArchivingCoreDataStorage:(CMMessageArchivingCoreDataStorage*)messageStorage didRecieveMessageComposingStatus:(BOOL)isTyping forJID:(NSString*)JID withStream:(XMPPStream*)stream;
@end
